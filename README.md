# Simple python3 app with Docker


```bash
cd /path/to/python-docker
pip3 install Flask
pip3 freeze | grep Flask >> requirements.txt
touch app.py
```

```python
from flask import Flask
app = Flask(__name__)

@app.route('/')
def hello_world():
    """some func"""
    return 'Hello, Docker!'
```

```bash
python3 -m flask run
```
